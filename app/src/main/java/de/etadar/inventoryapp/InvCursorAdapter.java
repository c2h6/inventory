/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.etadar.inventoryapp;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import de.etadar.inventoryapp.data.InvContract.InvEntry;

public class InvCursorAdapter extends CursorAdapter {

    public static final String TAG = InvCursorAdapter.class.getSimpleName();

    public InvCursorAdapter(Context context, Cursor c) {
        super(context, c, 0 /* flags */);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView nameTV = view.findViewById(R.id.name);
        TextView priceTV = view.findViewById(R.id.price);
        TextView quantTV = view.findViewById(R.id.quant);

        int nameColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_PRODUCT_NAME);
        int priceColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_PRODUCT_PRICE);
        int quantColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_PRODUCT_QUANTITY);
        int idColumnIndex = cursor.getColumnIndex(InvEntry._ID);

        String productName = cursor.getString(nameColumnIndex);
        Float productPrice = cursor.getFloat(priceColumnIndex);
        int productQuant = cursor.getInt(quantColumnIndex);
        final int id = cursor.getInt(idColumnIndex);

        nameTV.setText(productName);
        priceTV.setText(productPrice.toString());
        quantTV.setText(String.valueOf(productQuant));

        Button saleButton = view.findViewById(R.id.sale_button);
        saleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri currentProductUri = ContentUris.withAppendedId(InvEntry.CONTENT_URI, id);

                String[] projection = {
                        InvEntry._ID,
                        InvEntry.COLUMN_PRODUCT_QUANTITY };

                Cursor cursor = context.getContentResolver().query(
                        currentProductUri,           // Provider content URI to query
                        projection,                     // Columns to include in the resulting Cursor
                        null,               // No selection clause
                        null,                  // No selection arguments
                        null);           // Default sort order

                cursor.moveToNext(); // no error handling here, trust that exactly one response was returned

                int quantColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_PRODUCT_QUANTITY);
                int quant = cursor.getInt(quantColumnIndex);

                cursor.close();

                if (quant>0) {
                    quant--;

                    ContentValues values = new ContentValues();
                    values.put(InvEntry.COLUMN_PRODUCT_QUANTITY, quant);
                    context.getContentResolver().update(currentProductUri, values, null, null);
                    Toast.makeText(context,"Sold one product", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context,"No stock, no sale!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
