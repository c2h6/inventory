package de.etadar.inventoryapp;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import de.etadar.inventoryapp.data.InvContract.InvEntry;

public class EditorActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final int EXISTING_PRODUCT_LOADER = 0;
    private Uri mCurrentProductUri;
    private EditText mNameET;
    private EditText mPriceET;
    private EditText mQuantET;
    private EditText mSupNameET;
    private EditText mSupPhoneET;

    private boolean mProductHasChanged = false;

    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mProductHasChanged = true;
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        Intent intent = getIntent();
        mCurrentProductUri = intent.getData();

        if (mCurrentProductUri == null) {
            setTitle(getString(R.string.editor_activity_title_new_product));
            invalidateOptionsMenu();
        } else {
            setTitle(getString(R.string.editor_activity_title_edit_product));
            getLoaderManager().initLoader(EXISTING_PRODUCT_LOADER, null, this);
        }

        mNameET = findViewById(R.id.edit_product_name);
        mPriceET = findViewById(R.id.edit_product_price);
        mQuantET = findViewById(R.id.edit_product_quantity);
        mSupNameET = findViewById(R.id.edit_supplier_name);
        mSupPhoneET = findViewById(R.id.edit_supplier_phone);

        mNameET.setOnTouchListener(mTouchListener);
        mPriceET.setOnTouchListener(mTouchListener);
        mQuantET.setOnTouchListener(mTouchListener);
        mSupNameET.setOnTouchListener(mTouchListener);
        mSupPhoneET.setOnTouchListener(mTouchListener);

        Button decButton = findViewById(R.id.btn_less_quant);
        decButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int quant = Integer.parseInt(mQuantET.getText().toString());
                if (quant > 0) {
                    quant--;
                    mQuantET.setText(Integer.toString(quant));
                    mProductHasChanged = true;
                } else {
                    Toast.makeText(EditorActivity.this, "Quantity is already 0", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button incButton = findViewById(R.id.btn_more_quant);
        incButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quant = Integer.parseInt(mQuantET.getText().toString());
                mQuantET.setText(Integer.toString(quant+1));
                mProductHasChanged = true;
            }
        });

        Button delButton = findViewById(R.id.btn_del_product);
        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeleteConfirmationDialog();
            }
        });

        Button callButton = findViewById(R.id.btn_call_supplier);
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uri = ( "tel:" + mSupPhoneET.getText() ).trim();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(uri));
                startActivity(intent);
            }
        });
    }

        private boolean saveProduct() {
        String nameString = mNameET.getText().toString().trim();
        String priceString = mPriceET.getText().toString().trim();
        String quantString = mQuantET.getText().toString().trim();
        String supNameString = mSupNameET.getText().toString().trim();
        String supPhoneString = mSupPhoneET.getText().toString().trim();

        if (mCurrentProductUri == null &&
                TextUtils.isEmpty(nameString) && TextUtils.isEmpty(priceString) &&
                TextUtils.isEmpty(quantString) && TextUtils.isEmpty(supNameString) &&
                TextUtils.isEmpty(supPhoneString))  {
                    Toast.makeText(this, getString(R.string.editor_update_product_empty),
                    Toast.LENGTH_SHORT).show();
            return true;
                }

        if (TextUtils.isEmpty(nameString) || TextUtils.isEmpty(supNameString) || TextUtils.isEmpty(supPhoneString) ||
                TextUtils.isEmpty(quantString) || TextUtils.isEmpty(priceString)) {
            Toast.makeText(this, getString(R.string.editor_fields_empty),
                Toast.LENGTH_SHORT).show();
            return false;
        }

        ContentValues values = new ContentValues();
        values.put(InvEntry.COLUMN_PRODUCT_NAME, nameString);
        values.put(InvEntry.COLUMN_PRODUCT_SUPPLIER_NAME, supNameString);
        values.put(InvEntry.COLUMN_PRODUCT_SUPPLIER_PHONE, supPhoneString);

        int quantity = Integer.parseInt(quantString);
        values.put(InvEntry.COLUMN_PRODUCT_QUANTITY, quantity);

        float price = Float.parseFloat(priceString);
        values.put(InvEntry.COLUMN_PRODUCT_PRICE, price);

        if (mCurrentProductUri == null) {
            Uri newUri = getContentResolver().insert(InvEntry.CONTENT_URI, values);

            if (newUri == null) {
                Toast.makeText(this, getString(R.string.editor_insert_product_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.editor_insert_product_successful),
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            int rowsAffected = getContentResolver().update(mCurrentProductUri, values, null, null);

            if (rowsAffected == 0) {
                Toast.makeText(this, getString(R.string.editor_update_product_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.editor_update_product_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }
            return true;
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (mCurrentProductUri == null) {
            MenuItem menuItem = menu.findItem(R.id.action_delete);
            menuItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                boolean result = saveProduct();
                if (result) { finish(); }
                return true;
            case R.id.action_delete:
                showDeleteConfirmationDialog();
                return true;
            case android.R.id.home:
                if (!mProductHasChanged) {
                    NavUtils.navigateUpFromSameTask(EditorActivity.this);
                    return true;
                }

                DialogInterface.OnClickListener discardButtonClickListener =
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                NavUtils.navigateUpFromSameTask(EditorActivity.this);
                            }
                        };

                showUnsavedChangesDialog(discardButtonClickListener);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!mProductHasChanged) {
            super.onBackPressed();
            return;
        }

        DialogInterface.OnClickListener discardButtonClickListener =
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                };

        showUnsavedChangesDialog(discardButtonClickListener);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                InvEntry._ID,
                InvEntry.COLUMN_PRODUCT_NAME,
                InvEntry.COLUMN_PRODUCT_PRICE,
                InvEntry.COLUMN_PRODUCT_QUANTITY,
                InvEntry.COLUMN_PRODUCT_SUPPLIER_NAME,
                InvEntry.COLUMN_PRODUCT_SUPPLIER_PHONE};

        return new CursorLoader(this,   // Parent activity context
                mCurrentProductUri,         // Query the content URI for the current pet
                projection,             // Columns to include in the resulting Cursor
                null,                   // No selection clause
                null,                   // No selection arguments
                null);                  // Default sort order
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        if (cursor == null || cursor.getCount() < 1) {
            return;
        }

        if (cursor.moveToFirst()) {
            int nameColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_PRODUCT_NAME);
            int priceColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_PRODUCT_PRICE);
            int quantColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_PRODUCT_QUANTITY);
            int supNameColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_PRODUCT_SUPPLIER_NAME);
            int supPhoneColumnIndex = cursor.getColumnIndex(InvEntry.COLUMN_PRODUCT_SUPPLIER_PHONE);

            // Extract out the value from the Cursor for the given column index
            String name = cursor.getString(nameColumnIndex);
            float price = cursor.getInt(priceColumnIndex);
            int quant = cursor.getInt(quantColumnIndex);
            String supName = cursor.getString(supNameColumnIndex);
            String supPhone = cursor.getString(supPhoneColumnIndex);

            // Update the views on the screen with the values from the database
            mNameET.setText(name);
            mPriceET.setText(Float.toString(price));
            mQuantET.setText(Integer.toString(quant));
            mSupNameET.setText(supName);
            mSupPhoneET.setText(supPhone);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mNameET.setText("");
        mPriceET.setText("");
        mQuantET.setText("");
        mSupNameET.setText("");
        mSupPhoneET.setText("");
    }

    private void showUnsavedChangesDialog(
            DialogInterface.OnClickListener discardButtonClickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);
        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showDeleteConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteProduct();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteProduct() {
        if (mCurrentProductUri != null) {
            int rowsDeleted = getContentResolver().delete(mCurrentProductUri, null, null);
            if (rowsDeleted == 0) {
                Toast.makeText(this, getString(R.string.editor_delete_product_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.editor_delete_product_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }
        finish();
    }

}